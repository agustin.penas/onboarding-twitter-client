package com.etermax.onboarding.twitterkata.core.domain

data class Feed(val tweets: List<String>)