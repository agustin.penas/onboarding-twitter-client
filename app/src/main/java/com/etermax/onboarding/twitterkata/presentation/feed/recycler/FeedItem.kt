package com.etermax.onboarding.twitterkata.presentation.feed.recycler

data class FeedItem(val text: String)