package com.etermax.onboarding.twitterkata.presentation.feed

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etermax.onboarding.twitterkata.core.action.GetFeed
import com.etermax.onboarding.twitterkata.core.domain.Feed
import com.etermax.onboarding.twitterkata.rx.RxSchedulersRules
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Rule
import org.junit.Test

class FeedViewModelTest{

    private val getFeed: GetFeed = mock()
    private val feedViewModel = FeedViewModel(getFeed)

    @Rule
    @JvmField
    val rxSchedulersRule = RxSchedulersRules()

    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()


    @Test
    fun `should look for tweets when feed is visible`() {
        givenGetFeedNeverReturns()

        whenFeedIsVisible()

        thenGetFeedIsInvoked()
    }

    @Test
    fun `should show error when getting feed fails`() {
        givenGetFeedFails()

        whenFeedIsVisible()

        thenShowErrorIsNotNull()
    }

    @Test
    fun `should show feed when getFeed succeeds`() {
        givenGetFeedSucceeds(Feed(listOf("sarasa")))

        whenFeedIsVisible()

        thenFeedIsNotNull(Feed(listOf("sarasa")))
    }

    private fun thenFeedIsNotNull( feed: Feed) {
        assertThat(feedViewModel.feed.value).isEqualTo(feed)
    }

    private fun givenGetFeedSucceeds(feed: Feed) {
        whenever(getFeed.invoke()).thenReturn(Single.just(feed))
    }

    private fun givenGetFeedFails() {
        whenever(getFeed.invoke()).thenReturn(Single.error(IllegalStateException()))
    }

    private fun givenGetFeedNeverReturns() {
        whenever(getFeed.invoke()).thenReturn(Single.never())
    }

    private fun whenFeedIsVisible() {
        feedViewModel.onFeedVisible()
    }

    private fun thenGetFeedIsInvoked() {
        verify(getFeed).invoke()
    }

    private fun thenShowErrorIsNotNull() {
        assertThat(feedViewModel.showError.value).isNotNull
    }
}
