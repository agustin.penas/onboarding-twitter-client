package com.etermax.onboarding.twitterkata.core.action

import com.etermax.onboarding.twitterkata.infrastructure.networking.TweetService
import com.etermax.onboarding.twitterkata.infrastructure.representation.TweetRequest
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.observers.TestObserver
import org.junit.Test
import java.lang.IllegalStateException

class TweetTest {

    private val tweetService: TweetService = mock()

    private val tweet = Tweet(tweetService)

    private lateinit var tweetObserver: TestObserver<Void>

    @Test
    fun `should tweet through service`() {
        givenTweetWillNeverComplete()

        whenActionIsInvokedWith("Sarasa Cósmica")

        thenServiceIsInvokedWith("Sarasa Cósmica")
    }

    @Test
    fun `should succeed when service succeeds`() {
        givenTweetWillSucceed()

        whenActionIsInvokedWith("Sarasa Cósmica")

        thenTweetCompletes()
    }

    @Test
    fun `should fail when service fails`() {
        givenTweetWillFail()

        whenActionIsInvokedWith("Sarasa Cósmica")

        thenTweetFails()
    }

    private fun givenTweetWillNeverComplete() {
        whenever(tweetService.tweet(any())).thenReturn(Completable.never())
    }

    private fun givenTweetWillSucceed() {
        whenever(tweetService.tweet(any())).thenReturn(Completable.complete())
    }

    private fun givenTweetWillFail() {
        whenever(tweetService.tweet(any())).thenReturn(Completable.error(IllegalStateException()))
    }

    private fun whenActionIsInvokedWith(tweetText: String) {
        tweetObserver = tweet(tweetText).test()
    }

    private fun thenServiceIsInvokedWith(tweetText: String) {
        verify(tweetService).tweet(TweetRequest(tweetText))
    }

    private fun thenTweetCompletes() {
        tweetObserver.assertComplete()
    }

    private fun thenTweetFails() {
        tweetObserver.assertError { it is IllegalStateException }
    }
}