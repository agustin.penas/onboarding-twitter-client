package com.etermax.onboarding.twitterkata.core.action

import com.etermax.onboarding.twitterkata.core.domain.Feed
import com.etermax.onboarding.twitterkata.infrastructure.networking.TweetService
import io.reactivex.Single

class GetFeed(private val tweetService: TweetService) {

    operator fun invoke(): Single<Feed> {
        return tweetService.getFeed().map { Feed(it) }
    }
}
