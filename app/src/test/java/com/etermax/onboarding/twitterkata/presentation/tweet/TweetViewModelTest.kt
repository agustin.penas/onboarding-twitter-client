package com.etermax.onboarding.twitterkata.presentation.tweet

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etermax.onboarding.twitterkata.core.action.Tweet
import com.etermax.onboarding.twitterkata.rx.RxSchedulersRules
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import org.assertj.core.api.Assertions.assertThat
import org.junit.Rule
import org.junit.Test

class TweetViewModelTest {

    @Rule
    @JvmField
    val rxSchedulersRule = RxSchedulersRules()

    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()

    private val tweet: Tweet = mock()

    private val viewModel = TweetViewModel(tweet)

    @Test
    fun `should tweet through action`() {
        givenTweetNeverCompletes()

        whenTweeting("Sarasa Cósmica")

        thenActionIsInvokedWith("Sarasa Cósmica")
    }

    @Test
    fun `should show error if tweet fails`() {
        givenTweetFails()

        whenTweeting("Some tweet")

        thenErrorIsShown()
    }

    @Test
    fun `should show success if tweet succeeds`() {
        givenTweetSucceeds()

        whenTweeting("Some tweet")

        thenSuccessIsShown()
    }

    private fun givenTweetNeverCompletes() {
        whenever(tweet.invoke(any())).thenReturn(Completable.never())
    }

    private fun givenTweetFails() {
        whenever(tweet.invoke(any())).thenReturn(Completable.error(IllegalStateException()))
    }

    private fun givenTweetSucceeds() {
        whenever(tweet.invoke(any())).thenReturn(Completable.complete())
    }

    private fun whenTweeting(tweetText: String) {
        viewModel.onTweet(tweetText)
    }

    private fun thenActionIsInvokedWith(tweetText: String) {
        verify(tweet).invoke(tweetText)
    }

    private fun thenErrorIsShown() {
        assertThat(viewModel.showError.value).isNotNull
    }

    private fun thenSuccessIsShown() {
        assertThat(viewModel.showSuccess.value).isNotNull
    }
}