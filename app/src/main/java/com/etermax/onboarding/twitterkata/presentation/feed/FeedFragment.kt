package com.etermax.onboarding.twitterkata.presentation.feed

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.etermax.onboarding.twitterkata.R
import com.etermax.onboarding.twitterkata.core.domain.Feed
import com.etermax.onboarding.twitterkata.infrastructure.TweetInfrastructure
import com.etermax.onboarding.twitterkata.presentation.feed.recycler.FeedAdapter
import com.etermax.onboarding.twitterkata.presentation.feed.recycler.FeedItem
import kotlinx.android.synthetic.main.fragment_feed.*

class FeedFragment : Fragment(R.layout.fragment_feed) {

    private val viewModel by viewModels<FeedViewModel> {
        FeedViewModelFactory(TweetInfrastructure.getFeed)
    }
    private val feedAdapter by lazy { FeedAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        disableManualSwipe()
        setSwipeColorScheme()
        bindAdapter()
        bindFeed()
    }

    override fun onStart() {
        super.onStart()
        viewModel.onFeedVisible()
    }

    private fun setSwipeColorScheme() {
        feedSwipeRefresh.setColorSchemeResources(
            android.R.color.holo_red_dark,
            android.R.color.holo_orange_dark,
            android.R.color.holo_purple,
            android.R.color.holo_blue_light
        )
    }

    private fun disableManualSwipe() {
        feedSwipeRefresh.isEnabled = false
    }

    private fun bindAdapter() {
        feedRecycler.adapter = feedAdapter
    }

    private fun bindFeed() {
        viewModel.feed.observe(viewLifecycleOwner, Observer {
            feedAdapter.submitList(mapFeed(it))
        })
    }

    private fun mapFeed(feed: Feed): List<FeedItem>{
        return feed.tweets.map { FeedItem(it) }
    }


    companion object {
        fun newInstance() = FeedFragment()
    }

}