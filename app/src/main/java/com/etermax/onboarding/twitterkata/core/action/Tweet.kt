package com.etermax.onboarding.twitterkata.core.action

import com.etermax.onboarding.twitterkata.infrastructure.networking.TweetService
import com.etermax.onboarding.twitterkata.infrastructure.representation.TweetRequest
import io.reactivex.Completable

class Tweet(private val tweetService: TweetService) {

    operator fun invoke(tweetText: String): Completable {
        return tweetService.tweet(TweetRequest(tweetText))
    }

}
