package com.etermax.onboarding.twitterkata.infrastructure.networking

import com.etermax.onboarding.twitterkata.infrastructure.representation.TweetRequest
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT

interface TweetService {

    @PUT("users/juancito/tweets")
    fun tweet(@Body tweetRequest: TweetRequest): Completable

    @GET("users/juancito/tweets")
    fun getFeed(): Single<List<String>>

}