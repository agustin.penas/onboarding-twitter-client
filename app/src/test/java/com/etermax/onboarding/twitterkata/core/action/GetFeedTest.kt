package com.etermax.onboarding.twitterkata.core.action

import com.etermax.onboarding.twitterkata.core.domain.Feed
import com.etermax.onboarding.twitterkata.infrastructure.networking.TweetService
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Test
import java.lang.IllegalStateException

class GetFeedTest{

    private lateinit var feedObserver: TestObserver<Feed>
    private val tweetService: TweetService = mock()
    private val getFeed = GetFeed(tweetService)


    @Test
    fun `should get feed calling service`() {
        givenTweetServiceFeedNeverReturns()

        whenGettingFeed()

        thenServiceGetsFeed()
    }

    @Test
    fun `should fail when service fails`() {
        givenTweetServiceFeedFails()

        whenGettingFeed()

        thenGetFeedFails()
    }

    @Test
    fun `should succeed when service succeeds`() {
        givenTweetServiceFeedSucceeds(listOf("saraza", "frula"))

        whenGettingFeed()

        thenGetFeedReturns(Feed(listOf("saraza", "frula")))
    }

    private fun givenTweetServiceFeedSucceeds(tweets: List<String>) {
        whenever(tweetService.getFeed()).thenReturn(Single.just(tweets))
    }

    private fun givenTweetServiceFeedFails() {
        whenever(tweetService.getFeed()).thenReturn(Single.error(IllegalStateException()))
    }

    private fun givenTweetServiceFeedNeverReturns() {
        whenever(tweetService.getFeed()).thenReturn(Single.never())
    }

    private fun whenGettingFeed(){
        feedObserver = getFeed().test()
    }

    private fun thenServiceGetsFeed() {
        verify(tweetService).getFeed()
    }

    private fun thenGetFeedFails() {
        feedObserver.assertError { it is IllegalStateException }
    }

    private fun thenGetFeedReturns(feed: Feed) {
        feedObserver.assertValue(feed)
    }
}
