package com.etermax.onboarding.twitterkata.presentation.tweet

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.etermax.onboarding.twitterkata.core.action.Tweet

class TweetViewModelFactory(private val tweet: Tweet) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return TweetViewModel(tweet) as T
    }
}