package com.etermax.onboarding.twitterkata.presentation.tweet

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.etermax.onboarding.twitterkata.core.action.Tweet
import com.etermax.onboarding.twitterkata.presentation.common.livedata.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class TweetViewModel(private val tweet: Tweet) : ViewModel() {

    private val mutableShowSuccess = SingleLiveEvent<Unit>()
    val showSuccess: LiveData<Unit> = mutableShowSuccess

    private val mutableShowError = SingleLiveEvent<Unit>()
    val showError: LiveData<Unit> = mutableShowError

    private val subscriptions = CompositeDisposable()

    fun onTweet(tweetText: String) {
        tweet.invoke(tweetText)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    mutableShowSuccess.value = Unit
                },
                {
                    mutableShowError.value = Unit
                }
            ).apply { subscriptions.add(this) }
    }

    override fun onCleared() {
        subscriptions.dispose()
        super.onCleared()
    }

}
