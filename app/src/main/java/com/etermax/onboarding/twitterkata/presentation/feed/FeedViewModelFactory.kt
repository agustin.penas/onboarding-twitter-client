package com.etermax.onboarding.twitterkata.presentation.feed

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.etermax.onboarding.twitterkata.core.action.GetFeed

class FeedViewModelFactory(private val getFeed: GetFeed) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return FeedViewModel(getFeed) as T
    }
}