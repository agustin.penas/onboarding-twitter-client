package com.etermax.onboarding.twitterkata.presentation.tweet

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.etermax.onboarding.twitterkata.R
import com.etermax.onboarding.twitterkata.infrastructure.TweetInfrastructure
import kotlinx.android.synthetic.main.fragment_tweet.*

class TweetFragment : Fragment(R.layout.fragment_tweet) {

    private val viewModel by viewModels<TweetViewModel> {
        TweetViewModelFactory(TweetInfrastructure.tweet)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bindTweet()
        observeShowSuccess()
        observeShowError()
    }

    private fun observeShowSuccess() {
        viewModel.showSuccess.observe(viewLifecycleOwner, Observer {
            showSuccessToast()
            clearTweetEditText()
        })
    }

    private fun showSuccessToast() {
        Toast.makeText(requireContext(), "Success!", Toast.LENGTH_SHORT).show()
    }

    private fun observeShowError() {
        viewModel.showError.observe(viewLifecycleOwner, Observer {
            showErrorToast()
            clearTweetEditText()
        })
    }

    private fun showErrorToast() {
        Toast.makeText(requireContext(), "Error!", Toast.LENGTH_SHORT).show()
    }

    private fun clearTweetEditText() {
        tweetEditText.text.clear()
    }

    private fun bindTweet() {
        tweetButtonAction.setOnClickListener {
            viewModel.onTweet(tweetEditText.text.toString())
        }
    }

    companion object {
        fun newInstance() = TweetFragment()
    }

}