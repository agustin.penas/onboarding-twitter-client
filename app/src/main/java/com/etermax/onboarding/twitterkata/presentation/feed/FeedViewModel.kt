package com.etermax.onboarding.twitterkata.presentation.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etermax.onboarding.twitterkata.core.action.GetFeed
import com.etermax.onboarding.twitterkata.core.domain.Feed
import com.etermax.onboarding.twitterkata.presentation.common.livedata.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class FeedViewModel(private val getFeed: GetFeed) : ViewModel(){

    private val mutableFeed = MutableLiveData<Feed>()
    val feed: LiveData<Feed> = mutableFeed
    private val mutableShowError = SingleLiveEvent<Unit>()
    val showError: LiveData<Unit> = mutableShowError
    private val subscriptions = CompositeDisposable()


    fun onFeedVisible(){

        getFeed()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    mutableFeed.value = it
                },
                {
                    mutableShowError.value = Unit
                }
            ).apply { subscriptions.add(this) }
    }
}
