package com.etermax.onboarding.twitterkata.infrastructure

import com.etermax.onboarding.twitterkata.core.action.GetFeed
import com.etermax.onboarding.twitterkata.core.action.Tweet
import com.etermax.onboarding.twitterkata.infrastructure.networking.NetworkingInfrastructure

object TweetInfrastructure {

    val tweet = createTweet()
    val getFeed = createGetFeed()

    private fun createTweet() = Tweet(NetworkingInfrastructure.tweetService)

    private fun createGetFeed() = GetFeed(NetworkingInfrastructure.tweetService)

}