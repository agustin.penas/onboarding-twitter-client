package com.etermax.onboarding.twitterkata.presentation.feed.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.etermax.onboarding.twitterkata.R
import kotlinx.android.synthetic.main.view_holder_feed_item.view.*

class FeedAdapter : ListAdapter<FeedItem, FeedItemViewHolder>(FeedAdapterDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedItemViewHolder {
        return FeedItemViewHolder(parent)
    }

    override fun onBindViewHolder(holder: FeedItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

class FeedAdapterDiffCallback : DiffUtil.ItemCallback<FeedItem>() {

    override fun areItemsTheSame(oldItem: FeedItem, newItem: FeedItem) = oldItem == newItem

    override fun areContentsTheSame(oldItem: FeedItem, newItem: FeedItem) =
        areItemsTheSame(oldItem, newItem)

}

class FeedItemViewHolder(parent: ViewGroup) :
    RecyclerView.ViewHolder(inflateFeedViewHolder(parent)) {

    fun bind(item: FeedItem) {
        itemView.feedItemText.text = item.text
    }
}

private fun inflateFeedViewHolder(parent: ViewGroup) =
    LayoutInflater.from(parent.context).inflate(R.layout.view_holder_feed_item, parent, false)
