package com.etermax.onboarding.twitterkata.presentation.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.etermax.onboarding.twitterkata.R
import com.etermax.onboarding.twitterkata.presentation.feed.FeedFragment
import com.etermax.onboarding.twitterkata.presentation.tweet.TweetFragment
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment(R.layout.fragment_main) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bindFeedButton()
        bindTweetButton()
    }

    private fun bindFeedButton() {
        feedButton.setOnClickListener {
            goToFeed()
        }
    }

    private fun goToFeed() {
        parentFragmentManager.beginTransaction()
            .setCustomAnimations(
                android.R.anim.fade_in,
                android.R.anim.fade_out,
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
            .addToBackStack(null)
            .replace(R.id.container, FeedFragment.newInstance())
            .commit()
    }

    private fun bindTweetButton() {
        tweetButton.setOnClickListener {
            goToTweet()
        }
    }

    private fun goToTweet() {
        parentFragmentManager.beginTransaction()
            .setCustomAnimations(
                android.R.anim.fade_in,
                android.R.anim.fade_out,
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
            .addToBackStack(null)
            .replace(R.id.container, TweetFragment.newInstance())
            .commit()
    }

    companion object {
        fun newInstance() = MainFragment()
    }

}