package com.etermax.onboarding.twitterkata.infrastructure.representation

data class TweetRequest(val text: String)